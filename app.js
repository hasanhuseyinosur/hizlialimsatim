const path = require('path')
const express = require('express')
const alfa = require('express-handlebars')
const mongoose = require('mongoose')
const mongodb = require('mongodb')
const app = express()
const port = process.env.PORT || 5000
const bp = require('body-parser')
const CONNECTION_URL = "mongodb+srv://admin:adminpassword@realmcluster.elq5g.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"


mongoose
.connect(CONNECTION_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).then(()=>{
  app.listen(port, () =>{
    console.log('server is running')
  })
})
.catch((error) =>{
  console.error(error.message)
})

app.use(bp.json())
app.use(bp.urlencoded({ extended: true }))


app.engine('handlebars', alfa.engine({
  defaultLayout: "main",
  runtimeOptions: {
    allowProtoPropertiesByDefault: true,
    allowProtoMethodsByDefault: true,
  },
}))
app.set('view engine', 'handlebars');
app.set('views', './views');

app.use(express.static('public'))

const main = require('./routes/main')
app.use('/', main)

const users = require('./routes/iletisim')
app.use('/users', users)

const sikayet = require('./routes/sikayet')
app.use('/sikayet', sikayet)

const hakkimizda = require('./routes/hakkimizda')
app.use('/hakkimizda', hakkimizda)

const sss = require('./routes/sss')
app.use('/sss', sss)

const urunDegerlendir = require('./routes/urunDegerlendir')
app.use('/urunDegerlendir', urunDegerlendir)

const odeme = require('./routes/odeme')
app.use('/odeme', odeme)
