const express = require('express')
const router = express.Router()
const hakkimizda = require('../models/hakkimizda')
const sss = require('../models/sss')

  
  router.get('/iletisim', (req, res) => {
    res.render('site2/iletisim')
  })
  
  router.get('/', (req, res) => {
    res.render('site2/hakkimizda')
  })

  router.get('/odeme', (req, res) => {
    res.render('site2/odeme')
  })
  
 
  
  router.get('/sikayetFormu', (req, res) => {
    res.render('site2/sikayetFormu')
  })
  
  router.get('/urunDegerlendir', (req, res) => {
    res.render('site2/urunDegerlendir')
  })

  router.get('/hakkimizda', (req, res) =>{
    hakkimizda.find({}).then(hakkimizda =>{
      res.render('site2/hakkimizda', {hakkimizda:hakkimizda})
      console.log('success')
    })
  })

  router.get('/sss', (req, res) =>{
    sss.find({}).then(sss =>{
      res.render('site2/sss', {sss:sss})
      console.log('success')
    })
  })
  module.exports = router