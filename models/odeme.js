const mongoose = require('mongoose')

const odemeSchema = new mongoose.Schema({
    KartTürü:{ type: String, require: true },
    İsim:{ type: String, require: true },
    Tarih:{ type: String, require: true },
    Dogrulama:{ type: String, require: true },
    KartNumarasi:{ type: String, require: true }
})

module.exports = mongoose.model('odeme', odemeSchema)
