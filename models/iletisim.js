const mongoose = require('mongoose')

const iletisimSchema = new mongoose.Schema({
    İsim:{ type: String, require: true },
    Soyisim:{ type: String, require: true },
    Emailadresi:{ type: String, require: true },
    TelefonNumarası:{ type: String, require: true },
    Mesaj:{ type: String, require: true }
})

module.exports = mongoose.model('iletisim', iletisimSchema)
