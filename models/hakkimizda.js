const mongoose = require('mongoose')

const hakkimizdaSchema = new mongoose.Schema({
    id:{type: String, require: true},
    title:{ type: String, require: true },
    text1:{ type: String, require: true },
    text2:{ type: String, require: true }

})

module.exports = mongoose.model('hakkimizda', hakkimizdaSchema)