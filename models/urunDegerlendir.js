const mongoose = require('mongoose')

const urunDegerlendirSchema = new mongoose.Schema({
    
    rating:{ type: String, require: true },
    urunGonderildi:{ type: String, require: true },
    text:{ type: String, require: true }
    
})

module.exports = mongoose.model('urunDegerlendir', urunDegerlendirSchema)