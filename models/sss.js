const mongoose = require('mongoose')

const sssSchema = new mongoose.Schema({
    header:{ type: String, require: true },
    title:{ type: String, require: true },
    baslik1:{ type: String, require: true },
    icerik1:{ type: String, require: true },
    baslik2:{ type: String, require: true },
    icerik2:{ type: String, require: true },
    baslik3:{ type: String, require: true },
    icerik3:{ type: String, require: true },
    baslik4:{ type: String, require: true },
    icerik4:{ type: String, require: true },
    baslik5:{ type: String, require: true },
    icerik5:{ type: String, require: true }
    
})

module.exports = mongoose.model('sss', sssSchema)
