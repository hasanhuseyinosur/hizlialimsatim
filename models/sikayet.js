const mongoose = require('mongoose')

const sikayetSchema = new mongoose.Schema({
    adinizsoyadiniz:{ type: String, require: true },
    telefon:{ type: String, require: true },
    sikayet:{ type: String, require: true }
})

module.exports = mongoose.model('sikayet', sikayetSchema)